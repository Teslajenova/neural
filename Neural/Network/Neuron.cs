namespace Neural.Network{
  public class Neuron{
    
    private Dictionary<Neuron, NeuralFactor> input;
    
    private double output;
    
    private double biasInput;
    private NeuralFactor biasNeuralFactor;
    
    private double error;
    
    private Random rnd;
    
    public Neuron(Random rnd){
      this.rnd = rnd;
      
      biasInput = rnd.NextDouble();
      biasNeuralFactor = new NeuralFactor(rnd.NextDouble());
    }
    
    
    public void BindInputs(NeuralLayer layer){
      Dictionary<Neuron, NeuralFactor> newInput = new Dictionary<Neuron, NeuralFactor>();
      
      foreach(Neuron neuron in layer.Neurons){
        NeuralFactor factor = input[neuron];
        
        if(factor == null){
          factor = new NeuralFactor(rnd.NextDouble());
        }
        
        newInput.Add(neuron, factor);
      }
      
      input = newInput;
    }
    
    
    public void Pulse(){
      lock(this){

        output = 0;

        foreach(KeyValuePair<Neuron, NeuralFactor> item in input){

          output += item.Key.Output * item.Value.Weight;
        }
        
        output += biasInput * biasNeuralFactor.Weight;
        
        output = Sigmoid(output);
      }
    }
    
    
    public void ApplyLearning(NeuralLayer layer){
      //todo need implementing
    }
    
    
    private static double Sigmoid(double value){
      return 1 / (1 + Math.Exp(-value));
    }
    
    
    public double Output{
      get{
        return output;
      }
      set{
        output = value;
      }
    }
    
  }
}