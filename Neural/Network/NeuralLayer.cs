namespace Neural.Network{
  public class NeuralLayer{
	
    private List<Neuron> neurons = new List<Neuron>();
    
    
    public NeuralLayer(int neuronCount, Random rnd){
      AddNeurons(neuronCount, rnd);
    }
    
    
    public NeuralLayer(int neuronCount, Random rnd, NeuralLayer precedingLayer){
      AddNeurons(neuronCount, rnd);
      BindInputs(precedingLayer);
    }
    
    
    public void AddNeurons(int neuronCount, Random rnd){
      for(int i = 0; i < neuronCount; i++){
        neurons.Add(new Neuron(rnd));
      }
    }
    
    
    public void BindInputs(NeuralLayer precedingLayer){
      foreach(Neuron neuron in neurons){
        neuron.BindInputs(precedingLayer);
      }
    }
    
    
    public void Pulse(){
      lock(this){
        //todo use shuffled list
        foreach(Neuron neuron in neurons){
          neuron.Pulse();
        }
      }
    }

    
    public void ApplyLearning(NeuralNet net){
      foreach(Neuron neuron in neurons){
        neuron.ApplyLearning();
      }
    }
    
    
    public List<Neuron> Neurons{
      get{
        return neurons;
      }
    }

  }
}