namespace Neural.Network{
  public class NeuralFactor{
    
    private double weight;
    private double delta;
    
    
    public NeuralFactor(double weight){
      this.weight = weight;
      this.delta = 0;
    }
    
    
    public void ApplyDelta(){
      weight += delta;
      delta = 0;
    }
    
    
    public double Weight{
      get{
        return weight;
      }
    }
    
    
    public double Delta{
      get{
        return delta;
      }
    }
    
  }
}