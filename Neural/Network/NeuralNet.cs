namespace Neural.Network{
  public class NeuralNet{
    
    private Random rnd;
    
    private NeuralLayer perceptionLayer;
    
    private List<NeuralLayer> hiddenLayers = new List<NeuralLayer>();
    
    private NeuralLayer outputLayer;
    
    private double learningRate;
    // todo momentum
    
    
    public NeuralNet(int inputCount, int outputCount, double learningRate, int randomSeed){
      rnd = new Random(randomSeed);
      
      this.learningRate = learningRate;
      
      perceptionLayer = new NeuralLayer(inputCount, rnd);
      
      outputLayer = new NeuralLayer(outputCount, rnd, perceptionLayer);
    }
    
    
    public void AddHiddenLayer(int neuronCount){
      NeuralLayer precedingLayer;
      
      if(hiddenLayers.Count > 0){
        precedingLayer = hiddenLayers[hiddenLayers.Count -1];
      }else{
        precedingLayer = perceptionLayer;
      }
      
      NeuralLayer newLayer = new NeuralLayer(neuronCount, rnd, precedingLayer);
      
      hiddenLayers.Add(newLayer);
      
      outputLayer.BindInputs(newLayer);
    }
    
    
    public void Pulse(){
      lock(this){
        foreach(NeuralLayer layer in hiddenLayers){
          layer.Pulse();
        }
        outputLayer.Pulse();
      }
    }
    
    
    public void ApplyLearning(){
      
    }
    
    
    public void Train(){
      
    }
    
    
    public void BackPropagation(){
      
    }
    
  }
}