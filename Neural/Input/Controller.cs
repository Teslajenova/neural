namespace Neural.Input{
  public class Controller{
    
    private KeyboardState currentKBS;
    private KeyboardState previousKBS;
    
    private character character;
    
    public Controller(Character character){
      this.character = character;
    }
    
    public void Update(GameTime gameTime){
      previousKBS = currentKBS;
      currentKBS = Keyboard.GetState();
      
      if(previousKBS == null || currentKBS == null){
        // safety measure
        return;
      }
      
      ManipulateCharacter();
    }
    
    private void ManipulateCharacter(){
      if (currentKBS.
          IsKeyDown(Keys.Left)){
        character.Move(-1, 0);
      }
      
      if (currentKBS.
          IsKeyDown(Keys.Right)){
        character.Move(1, 0);
      }
      
      if (currentKBS.
          IsKeyDown(Keys.Up)){
        character.Move(0, -1);
      }
      
      if (currentKBS.
          IsKeyDown(Keys.Down)){
        character.Move(0, 1);
      }
      
    }
  
  }
}