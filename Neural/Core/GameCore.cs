﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Neural.Core {
  class GameCore {

    GraphicsDevice graphicsDevice;


    Character character1 = new Character(new Vector(30, 30), "dot");
    
    Controller controller = new Controller(character1);

    public GameCore(GraphicsDevice graphicsDevice){
      this.graphicsDevice = graphicsDevice;
    }

    public void Update(GameTime gameTime){
      controller.Update(gameTime);
      character1.Update(gameTime);
    }

    public void Draw(GameTime gameTime){
      SpriteBatch sBatch = new SpriteBatch(graphicsDevice);
      sBatch.Begin();

      character1.Draw(gameTime, sBatch);

      sBatch.End();
    }

  }
}