namespace Neural.Actor{
  public class Character{
  
    private Vector position;
    private Texture2D texture;
    
    public Character(Vector position, String textureName){
      this.position = position;
      this.texture = Content.Load<Texture2D>(textureName);
    }
    
    public void Move(float x, float y){
      //todo: make the move actually happen in update
      position = Vector.Add(position, new Vector(x, y));
    }
    
    public void Update(GameTime gameTime){
    
    }
    
    public void Draw(GameTime gameTime, SpriteBatch spriteBatch){
        spriteBatch.Draw(texture, position, Color.White);    
    }
  
  }
}